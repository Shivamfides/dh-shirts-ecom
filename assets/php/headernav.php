<!-- menu -->
<section class="menu-bar">
        <div class="navbar-fixed">
            <nav class="nav-wrapper">
                <div class="container">
                    <a href="#" class="brand-logo">Site Title</a>
                    <a href="#" class="sidenav-trigger" data-target="mobile-links">
                        <i class="material-icons">menu</i>
                    </a>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="">Home</a></li>
                        <li><a href="">Shop</a></li>
                        <li><a href="">About</a></li>
                        <li><a href="">Contact</a></li>
                        <li><a href="" class="btn black white-text">Log in</a></li>
                        <li><a href=""> <i class="large material-icons">shopping_cart</i></a></li>
                    </ul>
                </div>
            </nav>
        </div>

        <ul class="sidenav" id="mobile-links">
            <li><a href="">Home</a></li>
            <li><a href="">Shop</a></li>
            <li><a href="">About</a></li>
            <li><a href="">Contact</a></li>
            <li><a href="">Log in</a></li>
            <li><a href="">Cart</a></li>
        </ul>
    </section>