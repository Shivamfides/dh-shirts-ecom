<!-- footer -->
<section class="footer-sec">
        <footer class="page-footer">
            <div class="container">
                <div class="row">
                    <div class="col l6 m6 s12">
                        <h5 class="white-text"> Customer Care</h5>
                        <p class="grey-text text-lighten-4">Mon To sat - 9.00 AM To 6.00 AM <br> +91 0000000000 | +91
                            0000000000</p>
                        <div class="admin-mail">
                            <p>
                                dhsalesmail@gmail.com
                            </p>
                        </div>
                        <div class="social-icon">
                            <a href="" class="social-icon-pd"><i class="fab fa-facebook-f"></i></a>
                            <a href="" class="social-icon-pd"><i class="fab fa-instagram"></i></a>

                        </div>
                    </div>
                    <div class="col l6 m6 s12">
                        <h5 class="white-text"> Join Our Newsletter Now</h5>
                        <p class="grey-text text-lighten-4">Get E-mail updates about out lates shop and special offers.
                        </p>
                        <div class="row">
                            <div class="col s12 m12 l12">
                                <div class="input-field inline">
                                    <input id="email_inline" type="email" class="validate">
                                    <label class="email-label" for="email_inline">Enter Your Email</label><a
                                        class=" btn">subscribe</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            </div>
            <div class="tnc">
                <div class="container">
                    <div class="row">
                        <div class="col s12 m12 l12 center-align">
                            <p>Track order | Return & Exchange | Terms and Conditions | Pr\ivacy policy | Free Shipping
                                & Returns</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer-copyright">
                <div class="container">
                    <div class="row">
                        <div class="col s12 m6 l6">
                            <p>Copyright © 2020 All rights reserved</p>
                        </div>
                        <div class="col s12 m6 l6 payments">
                            <a class="grey-text text-lighten-4 right" href="#"><img src="images/payments/mc-symbol.png"
                                    alt=""></a>
                            <a class="grey-text text-lighten-4 right" href="#"><img src="images/payments/mc-symbol.png"
                                    alt=""></a>
                            <a class="grey-text text-lighten-4 right" href="#"><img src="images/payments/mc-symbol.png"
                                    alt=""></a>
                            <a class="grey-text text-lighten-4 right" href="#"><img src="images/payments/mc-symbol.png"
                                    alt=""></a>
                        </div>
                    </div>


                </div>
            </div>

        </footer>
    </section>