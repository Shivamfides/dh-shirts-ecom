<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/all.min.css">
    <link rel="stylesheet" href="css/ionicons.min.css">

    <!-- Slick CSS -->
    <link rel="stylesheet" type="text/css" href="css/slick.css" />
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css" />

    <title>Dh Shirt</title>
</head>

<body>
    <!-- menu -->
    <!-- <section class="menu-bar">
        <div class="navbar-fixed">
            <nav class="nav-wrapper">
                <div class="container">
                    <a href="#" class="brand-logo">Site Title</a>
                    <a href="#" class="sidenav-trigger" data-target="mobile-links">
                        <i class="material-icons">menu</i>
                    </a>
                    <ul class="right hide-on-med-and-down">
                        <li><a href="">Home</a></li>
                        <li><a href="">Shop</a></li>
                        <li><a href="">About</a></li>
                        <li><a href="">Contact</a></li>
                        <li><a href="" class="btn white indigo-text">Login in</a></li>
                    </ul>
                </div>
            </nav>
        </div>

        <ul class="sidenav" id="mobile-links">
            <li><a href="">Home</a></li>
            <li><a href="">Shop</a></li>
            <li><a href="">About</a></li>
            <li><a href="">Contact</a></li>
        </ul>
    </section> -->
    <?php include("assets/php/headernav.php"); ?>
    <!-- banner -->
    <section class="bnr">
        <div class="row">
            <div class="col s12 m12 l12 pd-0">
                <div class="bnr-slide">
                    <img src="images/banner_1.png" alt="">
                    <img src="images/banner_1.png" alt="">
                </div>
                <h1>FASHION</h1>
                <h2>
                    <span>Wear Your Dress</span>
                </h2>
                <div class="mouse">
                    <a href="#" class="mouse-icon">
                        <div class="mouse-wheel"><span class="ion-ios-arrow-down"></span></div>
                    </a>
                </div>
                <div class="overlay"></div>
            </div>
        </div>
    </section>

    <!-- our products -->
    <section class="container hom-products">
        <div class="row">
            <h3 class="col s12 center-align">OUR PRDUCTS</h3>
        </div>
        <div class="row">
            <div class="col s12 m4 l4">
                <div class="card">
                    <div class="card-image">
                        <img src="images/products/products-1.png">
                    </div>
                    <div class="card-content">
                        <span class="card-title">SHRESTAM</span>
                        <p>Round Neck Solid</p>
                        <span class="card-title">₹ 379</span>
                    </div>
                </div>
            </div>
            <div class="col s12 m4 l4">
                <div class="card">
                    <div class="card-image">
                        <img src="images/products/products-1.png">
                    </div>
                    <div class="card-content">
                        <span class="card-title">SHRESTAM</span>
                        <p>Round Neck Solid</p>
                        <span class="card-title">₹ 379</span>
                    </div>
                </div>
            </div>
            <div class="col s12 m4 l4">
                <div class="card">
                    <div class="card-image">
                        <img src="images/products/products-1.png">
                    </div>
                    <div class="card-content">
                        <span class="card-title">SHRESTAM</span>
                        <p>Round Neck Solid</p>
                        <span class="card-title">₹ 379</span>
                    </div>
                </div>
            </div>
            <div class="col s12 m4 l4">
                <div class="card">
                    <div class="card-image">
                        <img src="images/products/products-1.png">
                    </div>
                    <div class="card-content">
                        <span class="card-title">SHRESTAM</span>
                        <p>Round Neck Solid</p>
                        <span class="card-title">₹ 379</span>
                    </div>
                </div>
            </div>
            <div class="col s12 m4 l4">
                <div class="card">
                    <div class="card-image">
                        <img src="images/products/products-1.png">
                    </div>
                    <div class="card-content">
                        <span class="card-title">SHRESTAM</span>
                        <p>Round Neck Solid</p>
                        <span class="card-title">₹ 379</span>
                    </div>
                </div>
            </div>
            <div class="col s12 m4 l4">
                <div class="card">
                    <div class="card-image">
                        <img src="images/products/products-1.png">
                    </div>
                    <div class="card-content">
                        <span class="card-title">SHRESTAM</span>
                        <p>Round Neck Solid</p>
                        <span class="card-title">₹ 379</span>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <!-- trending -->
    <section class="container hom-trends">
        <div class="row">
            <h3 class="col s12 center-align">TRENDING</h3>
        </div>
        <div class="row pro-slid">
            <div class="col s12 m4 l4">
                <div class="card">
                    <div class="card-image">
                        <img src="images/products/products-1.png">
                        <span class="card-title">SHOP NOW</span>
                    </div>
                </div>
            </div>
            <div class="col s12 m4 l4">
                <div class="card">
                    <div class="card-image">
                        <img src="images/products/products-1.png">
                        <span class="card-title">SHOP NOW</span>
                    </div>
                </div>
            </div>
            <div class="col s12 m4 l4">
                <div class="card">
                    <div class="card-image">
                        <img src="images/products/products-1.png">
                        <span class="card-title">SHOP NOW</span>
                    </div>
                </div>
            </div>
            <div class="col s12 m4 l4">
                <div class="card">
                    <div class="card-image">
                        <img src="images/products/products-1.png">
                        <span class="card-title">SHOP NOW</span>
                    </div>
                </div>
            </div>
            <div class="col s12 m4 l4">
                <div class="card">
                    <div class="card-image">
                        <img src="images/products/products-1.png">
                        <span class="card-title">SHOP NOW</span>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <!-- fashion -->
    <section class="fashion">
        <div class="container">
            <div class="row">
                <div class="col s12 m5 l5">
                    <img src="images/online.png" alt="">
                </div>
                <div class="col s12 m7 l7 fashion-title">
                    <h3>MODIST ONLINE <br>FASHION SHOP</h3>
                    <p>On her way she met a copy. The copy warned the Little Blind Text,
                        that where it came from it would have been rewritten a thousand times and everything that was
                        left from
                        its origin would be the word "and" and the Little Blind Text should turn around and return to
                        its own,
                        safe country. But nothing the copy said could convince her and so it didn’t take long until a
                        few
                        insidious Copy Writers ambushed her, <br>made her drunk with Longe and Parole and dragged her
                        into their
                        agency, where they abused her for their.</p>
                    <div>
                    </div>
                </div>
    </section>

    <!-- testimony -->
    <section class="container testimony">
        <div class="row testimony-slid testimony-ht">
            <div class="col s12 m4 l4 testimony-ht">
                <div class="testimony-cntr">
                    <div class="testimony-image">
                        <img src="images/products/products-1.png">
                    </div>
                    <div class="card-content">
                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.
                            It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                        <span class="card-title">Sarah Ginn</span>
                    </div>
                </div>
            </div>
            <div class="col s12 m4 l4 testimony-ht">
                <div class="testimony-cntr">
                    <div class="testimony-image">
                        <img src="images/products/products-1.png">
                    </div>
                    <div class="card-content">
                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.
                            It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                        <span class="card-title">Sarah Ginn</span>
                    </div>
                </div>
            </div>
            <div class="col s12 m4 l4 testimony-ht">
                <div class="testimony-cntr">
                    <div class="testimony-image">
                        <img src="images/products/products-1.png">
                    </div>
                    <div class="card-content">
                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.
                            It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                        <span class="card-title">Sarah Ginn</span>
                    </div>
                </div>
            </div>
            <div class="col s12 m4 l4 testimony-ht">
                <div class="testimony-cntr">
                    <div class="testimony-image">
                        <img src="images/products/products-1.png">
                    </div>
                    <div class="card-content">
                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.
                            It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                        <span class="card-title">Sarah Ginn</span>
                    </div>
                </div>
            </div>
            <div class="col s12 m4 l4 testimony-ht">
                <div class="testimony-cntr">
                    <div class="testimony-image">
                        <img src="images/products/products-1.png">
                    </div>
                    <div class="card-content">
                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.
                            It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                        <span class="card-title">Sarah Ginn</span>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <!-- brands -->
    <section class="brands">
        <div class="brands-slid">
            <img src="images/brands/brands.png" alt="">
            <img src="images/brands/brands.png" alt="">
            <img src="images/brands/brands.png" alt="">
            <img src="images/brands/brands.png" alt="">
            <img src="images/brands/brands.png" alt="">
            <img src="images/brands/brands.png" alt="">
        </div>
    </section>

    <!-- footer -->
    <!-- <section class="footer-sec">
        <footer class="page-footer">
            <div class="container">
                <div class="row">
                    <div class="col l6 m6 s12">
                        <h5 class="white-text"> Customer Care</h5>
                        <p class="grey-text text-lighten-4">Mon To sat - 9.00 AM To 6.00 AM <br> +91 0000000000 | +91
                            0000000000</p>
                        <div class="admin-mail">
                            <p>
                                dhsalesmail@gmail.com
                            </p>
                        </div>
                        <div class="social-icon">
                            <a href="" class="social-icon-pd"><i class="fab fa-facebook-f"></i></a>
                            <a href="" class="social-icon-pd"><i class="fab fa-instagram"></i></a>

                        </div>
                    </div>
                    <div class="col l6 m6 s12">
                        <h5 class="white-text"> Join Our Newsletter Now</h5>
                        <p class="grey-text text-lighten-4">Get E-mail updates about out lates shop and special offers.
                        </p>
                        <div class="row">
                            <div class="col s12 m12 l12">
                                <div class="input-field inline">
                                    <input id="email_inline" type="email" class="validate">
                                    <label class="email-label" for="email_inline">Enter Your Email</label><a
                                        class=" btn">subscribe</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            </div>
            <div class="tnc">
                <div class="container">
                    <div class="row">
                        <div class="col s12 m12 l12 center-align">
                            <p>Track order | Return & Exchange | Terms and Conditions | Pr\ivacy policy | Free Shipping
                                & Returns</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="footer-copyright">
                <div class="container">
                    <div class="row">
                        <div class="col s12 m6 l6">
                            <p>Copyright © 2020 All rights reserved</p>
                        </div>
                        <div class="col s12 m6 l6 payments">
                            <a class="grey-text text-lighten-4 right" href="#"><img src="images/payments/mc-symbol.png"
                                    alt=""></a>
                            <a class="grey-text text-lighten-4 right" href="#"><img src="images/payments/mc-symbol.png"
                                    alt=""></a>
                            <a class="grey-text text-lighten-4 right" href="#"><img src="images/payments/mc-symbol.png"
                                    alt=""></a>
                            <a class="grey-text text-lighten-4 right" href="#"><img src="images/payments/mc-symbol.png"
                                    alt=""></a>
                        </div>
                    </div>


                </div>
            </div>

        </footer>
    </section> -->
     <?php include"assets/php/footer.php"; ?>
    <!-- Compiled and minified JavaScript -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>

    <!-- slick js -->
    <script type="text/javascript" src="js/slick.min.js"></script>
    <script>
    $(document).ready(function() {
        //nav bar
        $('.sidenav').sidenav();

        // main banner slider
        // trends
        $('.bnr-slide').slick({
            dots: true,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            arrows: true,
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        autoplay: true,
                        arrows: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: true,
                        autoplay: true,
                        dots: false
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: true,
                        autoplay: true,
                        dots: false
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
        // $('.bnr-slide').slick({
        //     dots: true,
        //     infinite: true,
        //     arrows: true,
        //     rtl: true,
        //     ltr: true,
        //     speed: 2000,
        //     slidesToShow: 1,
        //     slidesToScroll: 3
        // });


        // trends
        $('.pro-slid').slick({
            dots: true,
            infinite: true,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 3,
            autoplay: true,
            arrows: false,
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        autoplay: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        autoplay: true
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        autoplay: true
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });

        // testimonials
        $('.testimony-slid').slick({
            dots: true,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            arrows: false,
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        autoplay: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        autoplay: true
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        autoplay: true
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });

        // brands
        // testimonials
        $('.brands-slid').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            arrows: false,
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                        autoplay: true,
                        dots: false
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        autoplay: true
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        autoplay: true
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    });
    </script>
</body>

</html>